# README #

### What is this repository for? ###

* A Map-Reduce implementation for GHSOM

### How do I get set up? ###

* Create a Scala class and object file for your input dataset. Refer to the other class implementations inside com/ameyamm/mcs_thesis/input_generator folder.
* Place the dataset file at the corresponding location in HDFS as mentioned in the object file created above
* Run the object file

### Who do I talk to? ###

* ameyamm