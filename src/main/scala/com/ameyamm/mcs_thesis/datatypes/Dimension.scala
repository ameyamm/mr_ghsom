package com.ameyamm.mcs_thesis.datatypes

/**
 * @author ameya
 */
abstract class Dimension[T](protected var _value : T) extends DimensionType { 
  def value : T = _value
  def value_=(value : T) : Unit = { _value = value  }
 
  override def toString : String = {
    value.toString()
  }
}