package com.ameyamm.mcs_thesis.globals

/**
 * @author ameya
 */
object GHSomConfig {
  val EPOCHS = 15
  val INIT_LAYER_SIZE = 2
  val TAU1 = 0.85
  val TAU2 = 0.75
  val GRID_SIZE_FACTOR = 1.0
  val HIERARCHICAL_COUNT_FACTOR = 0.0005
  val LABEL_SOM = true
  val NUM_LABELS = 10
  val CLASS_LABELS = false
  val COMPUTE_TOPOGRAPHICAL_ERROR = true
  val DEBUG = false 
  val IGNORE_UNKNOWN = true // if true UNKNOWN values in categorical attributes are not considered as values
  val MQE_CRITERION = true
  val VARIANCE_METHOD = VarianceType.COEFF_UNALIKELIHOOD
  val GROWTH_MULTIPLE = true
  val LINK_WEIGHT = 0.5
}

object VarianceType extends Enumeration {
  type DimensionTypeEnum = Value
  val MQE_CRITERION, QE_CRITERION, SIMPLE_MATCHING, COEFF_UNALIKELIHOOD = Value
  //val NUMERIC, NOMINAL, ORDINAL, DISTANCE_HIERARCHY_NUMERIC, DISTANCE_HIERARCHY_NOMINAL = Value
}
