package com.ameyamm.mcs_thesis.globals

/**
 * @author ameya
 */

import org.apache.spark.SparkContext
import org.apache.spark.SparkConf

object SparkConfig {
  
  var sc : SparkContext = null
  
  def getSparkContext : SparkContext = {
    if (sc == null) {
      val conf = new SparkConf(true)
                    .setAppName("GHSOM Election Dataset")
                    .setMaster("local[4]")
                    .set("spark.cassandra.connection.host", "localhost")
                    .set("spark.cassandra.auth.username","ameya")
                    .set("spark.cassandra.auth.password","amu5886")

      sc = new SparkContext(conf)
    }
    sc 
  }
}