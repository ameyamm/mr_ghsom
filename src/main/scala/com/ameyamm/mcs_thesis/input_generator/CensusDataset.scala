package com.ameyamm.mcs_thesis.input_generator

/**
 * @author ameya
 */
import com.ameyamm.mcs_thesis.mr_ghsom.Instance
import com.ameyamm.mcs_thesis.datatypes.{DimensionTypeEnum,DistanceHierarchyDimension, DistanceHierarchyElem}
import com.ameyamm.mcs_thesis.datatypes.DimensionType
import com.ameyamm.mcs_thesis.mr_ghsom.GHSom
import com.ameyamm.mcs_thesis.globals.GHSomConfig
import com.ameyamm.mcs_thesis.mr_ghsom.Attribute
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.storage.StorageLevel
import org.apache.log4j.Logger
import org.apache.log4j.LogManager
import org.apache.commons
import org.apache.commons.io.FileUtils
import java.io.File
import scala.math.{max,min}

class CensusDataset (val dataset : RDD[String]) extends Serializable{
  
  val attributes = Array(
                        Attribute(name = "age", 
                                  index = 0, 
                                  dimensionType = DimensionTypeEnum.DISTANCE_HIERARCHY_NUMERIC,
                                  randomValueFunction = DistanceHierarchyDimension.getRandomDimensionValue),
                        Attribute(name = "class_of_worker", 
                                  index = 1, 
                                  dimensionType = DimensionTypeEnum.DISTANCE_HIERARCHY_NOMINAL,
                                  randomValueFunction = DistanceHierarchyDimension.getRandomDimensionValue), 
                       Attribute(name = "industry_code", 
                                  index = 2, 
                                  dimensionType = DimensionTypeEnum.DISTANCE_HIERARCHY_NOMINAL,
                                  randomValueFunction = DistanceHierarchyDimension.getRandomDimensionValue),
                       Attribute(name = "occupation_code", 
                                  index = 3, 
                                  dimensionType = DimensionTypeEnum.DISTANCE_HIERARCHY_NOMINAL,
                                  randomValueFunction = DistanceHierarchyDimension.getRandomDimensionValue),
                       Attribute(name = "education", 
                                  index = 4, 
                                  dimensionType = DimensionTypeEnum.DISTANCE_HIERARCHY_NOMINAL,
                                  randomValueFunction = DistanceHierarchyDimension.getRandomDimensionValue),
                       Attribute(name = "wage_per_hour", 
                                  index = 5, 
                                  dimensionType = DimensionTypeEnum.DISTANCE_HIERARCHY_NUMERIC,
                                  randomValueFunction = DistanceHierarchyDimension.getRandomDimensionValue),
                       Attribute(name = "enrolled_in_edu_inst_last_wk", 
                                  index = 6, 
                                  dimensionType = DimensionTypeEnum.DISTANCE_HIERARCHY_NOMINAL,
                                  randomValueFunction = DistanceHierarchyDimension.getRandomDimensionValue),
                       Attribute(name = "marital_status", 
                                  index = 7, 
                                  dimensionType = DimensionTypeEnum.DISTANCE_HIERARCHY_NOMINAL,
                                  randomValueFunction = DistanceHierarchyDimension.getRandomDimensionValue),
                       Attribute(name = "major_industry_code", 
                                  index = 8, 
                                  dimensionType = DimensionTypeEnum.DISTANCE_HIERARCHY_NOMINAL,
                                  randomValueFunction = DistanceHierarchyDimension.getRandomDimensionValue),
                       Attribute(name = "major_occupation_code", 
                                  index = 9, 
                                  dimensionType = DimensionTypeEnum.DISTANCE_HIERARCHY_NOMINAL,
                                  randomValueFunction = DistanceHierarchyDimension.getRandomDimensionValue),
                       Attribute(name = "race", 
                                  index = 10, 
                                  dimensionType = DimensionTypeEnum.DISTANCE_HIERARCHY_NOMINAL,
                                  randomValueFunction = DistanceHierarchyDimension.getRandomDimensionValue),
                       Attribute(name = "hispanic_origin", 
                                  index = 11, 
                                  dimensionType = DimensionTypeEnum.DISTANCE_HIERARCHY_NOMINAL,
                                  randomValueFunction = DistanceHierarchyDimension.getRandomDimensionValue),
                       Attribute(name = "sex", 
                                  index = 12, 
                                  dimensionType = DimensionTypeEnum.DISTANCE_HIERARCHY_NOMINAL,
                                  randomValueFunction = DistanceHierarchyDimension.getRandomDimensionValue),
                       Attribute(name = "member_of_a_labor_union", 
                                  index = 13, 
                                  dimensionType = DimensionTypeEnum.DISTANCE_HIERARCHY_NOMINAL,
                                  randomValueFunction = DistanceHierarchyDimension.getRandomDimensionValue),
                       Attribute(name = "reason_for_unemployment", 
                                  index = 14, 
                                  dimensionType = DimensionTypeEnum.DISTANCE_HIERARCHY_NOMINAL,
                                  randomValueFunction = DistanceHierarchyDimension.getRandomDimensionValue),
                       Attribute(name = "full_or_part_time_employment_stat", 
                                  index = 15, 
                                  dimensionType = DimensionTypeEnum.DISTANCE_HIERARCHY_NOMINAL,
                                  randomValueFunction = DistanceHierarchyDimension.getRandomDimensionValue),
                       Attribute(name = "capital_gains", 
                                  index = 16, 
                                  dimensionType = DimensionTypeEnum.DISTANCE_HIERARCHY_NUMERIC,
                                  randomValueFunction = DistanceHierarchyDimension.getRandomDimensionValue),
                       Attribute(name = "capital_losses", 
                                  index = 17, 
                                  dimensionType = DimensionTypeEnum.DISTANCE_HIERARCHY_NUMERIC,
                                  randomValueFunction = DistanceHierarchyDimension.getRandomDimensionValue),
                       Attribute(name = "divdends_from_stocks", 
                                  index = 18, 
                                  dimensionType = DimensionTypeEnum.DISTANCE_HIERARCHY_NUMERIC,
                                  randomValueFunction = DistanceHierarchyDimension.getRandomDimensionValue),
                       Attribute(name = "tax_filer_status", 
                                  index = 19, 
                                  dimensionType = DimensionTypeEnum.DISTANCE_HIERARCHY_NOMINAL,
                                  randomValueFunction = DistanceHierarchyDimension.getRandomDimensionValue),
                       Attribute(name = "region_of_previous_residence", 
                                  index = 20, 
                                  dimensionType = DimensionTypeEnum.DISTANCE_HIERARCHY_NOMINAL,
                                  randomValueFunction = DistanceHierarchyDimension.getRandomDimensionValue),
                       Attribute(name = "state_of_previous_residence", 
                                  index = 21, 
                                  dimensionType = DimensionTypeEnum.DISTANCE_HIERARCHY_NOMINAL,
                                  randomValueFunction = DistanceHierarchyDimension.getRandomDimensionValue),
                       Attribute(name = "detailed_household_and_family_stat", 
                                  index = 22, 
                                  dimensionType = DimensionTypeEnum.DISTANCE_HIERARCHY_NOMINAL,
                                  randomValueFunction = DistanceHierarchyDimension.getRandomDimensionValue),
                       Attribute(name = "detailed_household_summary_in_household", 
                                  index = 23, 
                                  dimensionType = DimensionTypeEnum.DISTANCE_HIERARCHY_NOMINAL,
                                  randomValueFunction = DistanceHierarchyDimension.getRandomDimensionValue),
                       Attribute(name = "live_in_this_house_1_year_ago", 
                                  index = 24, 
                                  dimensionType = DimensionTypeEnum.DISTANCE_HIERARCHY_NOMINAL,
                                  randomValueFunction = DistanceHierarchyDimension.getRandomDimensionValue),
                       Attribute(name = "num_persons_worked_for_employer", 
                                  index = 25, 
                                  dimensionType = DimensionTypeEnum.DISTANCE_HIERARCHY_NUMERIC,
                                  randomValueFunction = DistanceHierarchyDimension.getRandomDimensionValue),
                       Attribute(name = "family_members_under_18", 
                                  index = 26, 
                                  dimensionType = DimensionTypeEnum.DISTANCE_HIERARCHY_NOMINAL,
                                  randomValueFunction = DistanceHierarchyDimension.getRandomDimensionValue),
                       Attribute(name = "country_of_birth_father", 
                                  index = 27, 
                                  dimensionType = DimensionTypeEnum.DISTANCE_HIERARCHY_NOMINAL,
                                  randomValueFunction = DistanceHierarchyDimension.getRandomDimensionValue),
                       Attribute(name = "country_of_birth_mother", 
                                  index = 28, 
                                  dimensionType = DimensionTypeEnum.DISTANCE_HIERARCHY_NOMINAL,
                                  randomValueFunction = DistanceHierarchyDimension.getRandomDimensionValue),
                       Attribute(name = "country_of_birth_self", 
                                  index = 29, 
                                  dimensionType = DimensionTypeEnum.DISTANCE_HIERARCHY_NOMINAL,
                                  randomValueFunction = DistanceHierarchyDimension.getRandomDimensionValue),
                       Attribute(name = "citizenship", 
                                  index = 30, 
                                  dimensionType = DimensionTypeEnum.DISTANCE_HIERARCHY_NOMINAL,
                                  randomValueFunction = DistanceHierarchyDimension.getRandomDimensionValue),
                       Attribute(name = "own_business_or_self_employed", 
                                  index = 31, 
                                  dimensionType = DimensionTypeEnum.DISTANCE_HIERARCHY_NOMINAL,
                                  randomValueFunction = DistanceHierarchyDimension.getRandomDimensionValue),
                       Attribute(name = "fill_inc_questionnaire_for_veteran_admin", 
                                  index = 32, 
                                  dimensionType = DimensionTypeEnum.DISTANCE_HIERARCHY_NOMINAL,
                                  randomValueFunction = DistanceHierarchyDimension.getRandomDimensionValue),
                       Attribute(name = "veterans_benefits", 
                                  index = 33, 
                                  dimensionType = DimensionTypeEnum.DISTANCE_HIERARCHY_NOMINAL,
                                  randomValueFunction = DistanceHierarchyDimension.getRandomDimensionValue),
                       Attribute(name = "weeks_worked_in_year", 
                                  index = 34, 
                                  dimensionType = DimensionTypeEnum.DISTANCE_HIERARCHY_NUMERIC,
                                  randomValueFunction = DistanceHierarchyDimension.getRandomDimensionValue),
                       Attribute(name = "income_class", 
                                  index = 35, 
                                  dimensionType = DimensionTypeEnum.DISTANCE_HIERARCHY_NOMINAL,
                                  randomValueFunction = DistanceHierarchyDimension.getRandomDimensionValue)
                      )
  
  
    private val datasetOfInstanceObjs = instanizeDataset(dataset)
  
  case class Data ( className : String, attributeVector : Array[DimensionType] ) {
    def getInstanceObj = {
      Instance(className, attributeVector)
    }
  }
  
  def printDataset() {
    // println(datasetRDD.count)
  }
  
  def getDataset : RDD[Instance] = datasetOfInstanceObjs
  
  private def instanizeDataset(dataset : RDD[String]) : RDD[Instance] = {

    // RDD[PureData] i.e. RDD[class-name, <attribute-vector>]
    val pureDataRDD = getPureDataRDD(dataset) 

    // RDD[PureData] => RDD[(index, AttributeTypeValue[AttributeType, Value])]
    val attributeMap = getIndexAttributeTypeValueRDD( pureDataRDD )   
                                           
    val (minAttributeMap, maxAttributeMap) = getAttributeMinMaxValuesMap(attributeMap)
    
    val domainAttributeMap = getAttributeDomainValuesMap(attributeMap) 
    
    for (i <- 0 until attributes.size) {
      attributes(i).dimensionType match {
        case DimensionTypeEnum.DISTANCE_HIERARCHY_NOMINAL =>
          attributes(i).domainValues = domainAttributeMap(i).filter { value => !value.equals("UNKNOWN") }
                                                            .toArray
                                                            .sorted
        case DimensionTypeEnum.DISTANCE_HIERARCHY_NUMERIC =>
          attributes(i).maxValue = maxAttributeMap(i)
          attributes(i).minValue = minAttributeMap(i)
        case _ => throw new UnsupportedOperationException("Unsupported DimensionType")
      }
    }
    
    getInstanceRDD(pureDataRDD)
  }    

  // Pure record from the file (just split the class-name and attribute vector)
  private case class PureData(name : String, attributeVector : Array[String])
  // Class for holding just a attribute type and the original string value
  private case class AttributeTypeValue (attributeType : DimensionTypeEnum.Value, value : String)
  
  private def getPureDataRDD(dataset : RDD[String]) = {
    dataset.map{
      record => {
        val array = record.split(',')
        PureData(
            "Dummy",
            Array.tabulate(attributes.size)(i => array(i))
        )
      }
    }
  }
  
  private def getIndexAttributeTypeValueRDD ( pureDataRDD : RDD[PureData]) = {

    def convertToIndexAttributeTypeValueTuple(index : Int, value : String) : (Int, AttributeTypeValue)= {
      attributes(index).dimensionType match {
         case DimensionTypeEnum.DISTANCE_HIERARCHY_NUMERIC =>
           (index, AttributeTypeValue(DimensionTypeEnum.DISTANCE_HIERARCHY_NUMERIC, value))
         case DimensionTypeEnum.DISTANCE_HIERARCHY_NOMINAL =>
           (index, AttributeTypeValue(DimensionTypeEnum.DISTANCE_HIERARCHY_NOMINAL, value))
         case _ => throw new UnsupportedOperationException("Unsupported DimensionType")
       }
    }

    pureDataRDD.flatMap { 
      pureData => 
        pureData.attributeVector.zipWithIndex
                                .map(valueIndex => convertToIndexAttributeTypeValueTuple(valueIndex._2, valueIndex._1)) 
    }
  }
  
  private def getAttributeMinMaxValuesMap( inputRDD : RDD[(Int, AttributeTypeValue)]) = {
    val numericAttributesRDD = inputRDD.filter(tuple => tuple._2.attributeType == DimensionTypeEnum.DISTANCE_HIERARCHY_NUMERIC)
                                       .map(tuple => (tuple._1, tuple._2.value.toDouble))
    numericAttributesRDD.persist(StorageLevel.MEMORY_ONLY)
    
    val maxAttributeMap = numericAttributesRDD.reduceByKey( max(_,_) ).collectAsMap()

    val minAttributeMap = numericAttributesRDD.reduceByKey( min(_,_) ).collectAsMap()
    
    numericAttributesRDD.unpersist()

    (minAttributeMap, maxAttributeMap)
  }
  
  private def getAttributeDomainValuesMap( inputRDD : RDD[(Int, AttributeTypeValue)]) = {
    val nominalAttributesRDD = inputRDD.filter(tuple => tuple._2.attributeType == DimensionTypeEnum.DISTANCE_HIERARCHY_NOMINAL)
                                           .map(tuple => (tuple._1, Set(tuple._2.value)))
    nominalAttributesRDD.reduceByKey( _.union(_) ).collectAsMap()
  }
    
  private def getInstanceRDD( inputRDD : RDD[PureData] ) : RDD[Instance] = {
    
    def getDistanceHierarchyDimension(index : Int, value : String) = {
      
      attributes(index).dimensionType match {

        case DimensionTypeEnum.DISTANCE_HIERARCHY_NOMINAL => {
          val distanceHierarchyElemObj = DistanceHierarchyElem(value, 0.5)  
          DistanceHierarchyDimension(attributes(index).name, distanceHierarchyElemObj, DimensionTypeEnum.DISTANCE_HIERARCHY_NOMINAL)
        }

        case DimensionTypeEnum.DISTANCE_HIERARCHY_NUMERIC => {
          val normalizedValue = (value.toDouble - attributes(index).minValue) / (attributes(index).maxValue - attributes(index).minValue)
          val distanceHierarchyElemObj = DistanceHierarchyElem("+", normalizedValue)  
          DistanceHierarchyDimension(attributes(index).name, distanceHierarchyElemObj, DimensionTypeEnum.DISTANCE_HIERARCHY_NUMERIC)
        }

        case _ => throw new UnsupportedOperationException("Unsupported DimensionType")
      }
    }
    
    inputRDD.map{ 
      record => {
        val attributeVectorIndex = record.attributeVector.zipWithIndex  
        Data(
          record.name,
          attributeVectorIndex.map{ 
            valueIndex => getDistanceHierarchyDimension(valueIndex._2, valueIndex._1)
          }
        )
        .getInstanceObj
      }
    } 
  }
}

object CensusDataset {
  
  val logger = LogManager.getLogger("CensusDataset")
  
  def main(args : Array[String]) {
    val conf = new SparkConf(true)
               .setAppName("Census")
               //.set("spark.storage.memoryFraction","0")
               .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
               .set("spark.default.parallelism","8")
               .set("spark.kryoserializer.buffer.max","400")
               .setMaster("spark://192.168.101.13:7077")
               .set("spark.driver.memory", "4g")
               .set("spark.executor.memory","4g")

    val sc = new SparkContext(conf) 
    
    var epochs = GHSomConfig.EPOCHS
    if(args.length >= 1) {
      epochs = args(0).toInt  
    }
    
    println("EPOCHS: " + epochs)
    println("TAU1: " + GHSomConfig.TAU1)
    println("TAU2: " + GHSomConfig.TAU2)

    /*
    val maxVector = Array.fill(10)(DoubleDimension.MinValue)
    val attribVector = Array.fill(10)(DoubleDimension.getRandomDimensionValue)
    println(maxVector.mkString)
    println(attribVector.mkString)

    for ( i <- 0 until attribVector.size ) { 
        maxVector(i) = if (attribVector(i) > maxVector(i)) attribVector(i) else maxVector(i)
    } 
    println(maxVector.mkString)
    */

    val dataset = sc.textFile("hdfs://192.168.101.13:9000/user/ameya/datasets/census_income/census_income.data")
    val datasetReader = new CensusDataset(dataset) 
    datasetReader.printDataset()
    val processedDataset = datasetReader.getDataset
    
    val ghsom = GHSom()
    
    ghsom.train(processedDataset, datasetReader.attributes, epochs)
  }
}
